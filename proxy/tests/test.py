import os
from proxy.answer_processor import AnswerProcessor


def test_output():
    paths = list(os.path.split(__file__))
    paths[-1] = "run.py"
    path = os.path.join(*paths)
    first = open(path)

    paths = list(os.path.split(__file__))
    paths[-1] = "answer_processor.py"
    path = os.path.join(*paths)
    second = open(path)

    answer_processor = AnswerProcessor([["first", first.read()], ["second", second.read()]],
                                       "Pyro4.config.SERIALIZER = config[\"serializer\"]\n"
                                       "Pyro4.config.SERIALIZERS_ACCEPTED.add(config[\"serializer\"])")
    preduce_return = [["first", "    Pyro4.config.SERIALIZER = config[\"serializer\"]\n    Pyro4.config."
                                "SERIALIZERS_ACCEPTED.add(config[\"serializer\"])\n\n    daemon = Pyro4.Daemon"
                                "(host=config[\"host\"], port=config[\"port\"])"],
                      ["second", "            snippet_start_index = self._get_snippet_start_index(tokenized_content)"
                                 "\n            snippet = self._get_snippet(snippet_start_index,"
                                 " tokenized_content)\n            result.append([url, "
                                 "snippet])\n        return result"]]

    assert (answer_processor.build_answer()[1][1] == preduce_return[1][1])
